package ru.hmw.Repositories;

import ru.hmw.Models.File;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;
import java.util.List;

public class FilesRepositoryJPAImpl implements FilesRepository {

    private final EntityManager entityManager;

    public FilesRepositoryJPAImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(File file) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(file);
        transaction.commit();
    }

    @Override
    public File findByFileName_name(String name) {
        TypedQuery<File> query = entityManager.createQuery("select file from File file " +
                "where file.originalFileName = :name", File.class);
        query.setParameter("name", name);
        return query.getSingleResult();
    }

    @Override
    public List<File> findAllByAccount_email(String email) {
        TypedQuery<File> query = entityManager.createQuery("select file from File file " +
                "left join file.account account where account.email = :email", File.class);
        query.setParameter("email", email);
        return query.getResultList();
    }
}
