package ru.hmw.Repositories;

import ru.hmw.Models.Account;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class AccountsRepositoryJpaImpl implements AccountsRepository {

    private final EntityManager entityManager;

    public AccountsRepositoryJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(Account account) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(account);
        transaction.commit();
    }
}
