package ru.hmw.Repositories;

import ru.hmw.Models.File;

import java.util.List;

public interface FilesRepository {
    void save(File file);

    File findByFileName_name(String name);

    List<File> findAllByAccount_email(String email);
}
