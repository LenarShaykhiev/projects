package ru.hmw.Repositories;

import ru.hmw.Models.Account;

public interface AccountsRepository {
    public void save(Account account);
}
