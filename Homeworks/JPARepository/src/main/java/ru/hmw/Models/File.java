package ru.hmw.Models;

import lombok.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.StringJoiner;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

@Entity
public class File {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "original_name")
    private String originalFileName;
    @Column(name = "storage_name")
    private String storageFileName;

    private Long size;
    @Column(name = "mime_type")
    private String mimeType;

    private String description;
    @ManyToOne()
    @JoinColumn(name = "account_id")
    private Account account;

    @Override
    public String toString() {
        return "File{" +
                "id=" + id +
                ", originalFileName='" + originalFileName + '\'' +
                ", storageFileName='" + storageFileName + '\'' +
                ", size=" + size +
                ", mimeType='" + mimeType + '\'' +
                ", description='" + description + '\'' +
                ", account=" + account +
                '}';
    }
}

