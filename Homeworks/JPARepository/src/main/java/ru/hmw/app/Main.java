package ru.hmw.app;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import ru.hmw.Models.Account;
import ru.hmw.Models.File;
import ru.hmw.Repositories.AccountsRepository;
import ru.hmw.Repositories.AccountsRepositoryJpaImpl;
import ru.hmw.Repositories.FilesRepository;
import ru.hmw.Repositories.FilesRepositoryJPAImpl;

import javax.persistence.EntityManager;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();
        EntityManager entityManager = sessionFactory.createEntityManager();
        FilesRepository filesRepository = new FilesRepositoryJPAImpl(entityManager);
        AccountsRepository accountsRepository = new AccountsRepositoryJpaImpl(entityManager);

        Account lenar = Account.builder()
                .firstName("1Sergey")
                .lastName("Shaykhiev")
                .email("1sergeyka@mail.ru")
                .password("123456")
                .build();

        accountsRepository.save(lenar);

        File firstFile = File.builder()
                .originalFileName("1firstFile")
                .storageFileName("jkbidjnik24g")
                .description("it's a first file")
                .mimeType(".img")
                .size(234L)
                .account(lenar)
                .build();

        File secondFile = File.builder()
                .originalFileName("1secondFile")
                .storageFileName("qwrgnas45fbg")
                .description("it's a second file")
                .mimeType(".img")
                .size(20L)
                .account(lenar)
                .build();

        filesRepository.save(firstFile);
        filesRepository.save(secondFile);

        System.out.println(filesRepository.findByFileName_name("firstFile"));
        System.out.println(filesRepository.findAllByAccount_email("sergeyka@mail.ru"));
    }
}
