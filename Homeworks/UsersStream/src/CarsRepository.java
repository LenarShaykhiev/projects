import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarsRepository {

    private final String fileName;

    private List<Car> cars;

    public CarsRepository(String fileName) {
        this.fileName = fileName;
    }

    final static Function<String, Car> carMapFunction = line -> {
        String[] parts = line.split("\\|");

        String number = parts[0];
        String model = parts[1];
        String color = parts[2];
        Integer mileage = Integer.parseInt(parts[3]);
        long price = Long.parseLong(parts[4]);

        return new Car(number, model, color, mileage, price);
    };

    public List<Car> findAll() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            return reader.lines().map(carMapFunction).collect(Collectors.toList());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public void getNumberOfColorAndMileageFilter(List<Car> carList, String color, Integer mileage) {
        carList.stream()
                .filter(car -> car.getColor().equalsIgnoreCase(color) || car.getMileage() == mileage)
                .map(Car::getNumber)
                .forEach(System.out::println);


    }

    public long getCountOfFilteredToPrice(List<Car> carList, int min, int max) {
        return carList.stream()
                .mapToLong(Car::getPrice)
                .distinct()
                .filter(price -> price >= min && price <= max)
                .count();
    }

    public void getColorOfMinPrice(List<Car> carList) {
        carList.stream()
                .min(Comparator.comparing(Car::getPrice))
                .map(Car::getColor)
                .ifPresent(System.out::println);
    }

    public void gerAveragePriceOfModel(List<Car> carList, String model) {
        carList.stream()
                .filter(car -> car.getModel().equalsIgnoreCase(model))
                .mapToDouble(Car::getPrice)
                .average()
                .ifPresent(System.out::println);


    }

}
